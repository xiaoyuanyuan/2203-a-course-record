### 课堂笔记

### 1、即席查询

![image-20241104142833991](https://88tuchuang88.oss-cn-beijing.aliyuncs.com/img/image-20241104142833991.png)

1.Druid:是一个实时处理时序数据的OLAP数据库，因为它的索引首先按照时间分片，查询的时候也是按照时间线去路由索引。
2.Kylin:核心是CUbe，Cube是一种预计算技术，基本思路是预先对数据作多维索引，查询时只扫描索引而不访问原始数据从而提速。
3.Presto:它没有使用Mapreduce，大部分场景下比HIVE块一个数量级，其中的关键是所有的处理都在内存中完成。
4.Impala:基于内存计算，速度快，支持的数据源没有Presto多。
5.SparkSQL：是spark用来处理结构化的一个模块，它提供一个抽象的数据集DataFrame,并且是作为分布式SQL查询引擎的应用。它还可以实现Hive on Spark,hive里的数据用sparksql查询。
6.框架选型：(1)从超大数据的查询效率来看：
Druid>Kylin>Presto>SparkSQL
(2)从支持的数据源种类来讲：
Presto>SparkSQL>Kylin>Druid

#### Kylin4.0 VS 3.0

```
1、引擎由MR变成Spark
2、cube存储由Hbase变成parquet文件，直接存储到Hadoop上
```





```
create view dim_user_info_view as select * from dim_user_zip where dt='9999-12-31';
```



```
create view dim_sku_info_view
as
select
    id,
    price,
    sku_name,
    sku_desc,
    weight,
    is_sale,
    spu_id,
    spu_name,
    category3_id,
    category3_name,
    category2_id,
    category2_name,
    category1_id,
    category1_name,
    tm_id,
    tm_name,
    create_time
from dim_sku_full
where dt="2022-06-08";
```



```
create view dim_province_view
as
select
   *
from dim_province_full
where dt="2022-06-08";
```

