# 课堂笔记

### 1、SQL优化

```
explain select
    coupon_id,
    count(*)
from dwd_trade_order_detail_inc
where dt='2022-06-08'
group by coupon_id;
```

### 2、Group by优化

```
开启Map端聚合，减少shuffle数据量，Map默认开启的
```

### 3、Join

```
小表 Join 小表: 不需要优化

小表 Join 大表:开启Map Join

大表 Join 大表：common join


分桶表Join：要保证分通的字段和Join的表一直，在Join能直接定位数据在哪桶中


common join  就是reduce join
Map join 没有reduce ,没有reduce 就没有shuffle
```

### 4、数据倾斜

```
分组引起的数据倾斜：
				1、开启Map端聚合
				2、skew groupby，开启两个reduce 默认不开启
				其原理是启动两个MR任务，第一个MR按照随机数分区，将数据分散发送到Reduce，完成部分聚合，第二个MR按照分组字段分区，完成最终聚合。
				
				
Join以前的数据倾斜：
				1、开启Map join
				2、开启skew join，默认不开启
					分别处理小Key,大Key，skew join只支持Inner Join
```

### 5、并行度

```
Map个数
	--可将多个小文件切片，合并为一个切片，进而由一个map任务处理
set hive.input.format=org.apache.hadoop.hive.ql.io.CombineHiveInputFormat; 
--一个切片的最大值
set mapreduce.input.fileinputformat.split.maxsize=256000000;



Reduce个数
--指定Reduce端并行度，默认值为-1，表示用户未指定
set mapreduce.job.reduces;
--Reduce端并行度最大值
set hive.exec.reducers.max;
--单个Reduce Task计算的数据量，用于估算Reduce并行度
set hive.exec.reducers.bytes.per.reducer;
```



```
让Hive把表大小算的更准一下

--执行DML语句时，收集表级别的统计信息
set hive.stats.autogather=true;
--执行DML语句时，收集字段级别的统计信息
set hive.stats.column.autogather=true;
--计算Reduce并行度时，从上游Operator统计信息获得输入数据量
set hive.spark.use.op.stats=true;
--计算Reduce并行度时，使用列级别的统计信息估算输入数据量
set hive.stats.fetch.column.stats=true;
```

### 6、小文件优化优化

```
--开启合并Hive on Spark任务输出的小文件
set hive.merge.sparkfiles=true;  默认不开启
```

### 7、分点答

```
1、SQL遵守规则
2、资源优化
3、SQL优化，SQL是否要优化，explain
```

### 8、Zabbix







### 9、总结

```
SQL基础
	1、采集框架：Flume、Datax(DataXWeb)、Maxwell、Sqoop
	2、消息队列：Kafka
	3、SQL:Hive
	4、数仓理论：
	5、可视化:FineReport,FineBI
	6、调度： 海豚、Azkanban
	7、优化：
	8、数据治理：Python脚本、DataVines
	9、即席查询：Kylin、Presto
	10、业务：电商		  			 
```

