### 课堂笔记

### 1、可视化图表

```
SuperSet:开源的可视化组件
apache echarts：需要前端工程师
Pyecharts:
		https://gallery.pyecharts.org/
```

### 2、BI(商业智能)

```
FineBI:帆软BI
永洪BI:国产BI
dataease:飞致云
PowerBI:
Tableau:
Quick BI:
```

### 3、帆软

```
FineReport:注重前端展示效果

FineBI:以业务为中心的自助分析平台 

https://www.finereport.com/report/fr-finereportfinebiqb.html
```

![image-20241031084743290](https://88tuchuang88.oss-cn-beijing.aliyuncs.com/img/image-20241031084743290.png)

### 4、FineReport

```
链接数据库会报错---换驱动
```

### 5、报表类型

![image-20241031104905530](C:\Users\xiaoyuan\AppData\Roaming\Typora\typora-user-images\image-20241031104905530.png)

```
https://blog.csdn.net/yuanziok/article/details/139127461
```

```
FVS可视化看板
在 FVS 之前，制作驾驶舱可使用 决策报表 模式，但推荐使用 FVS ，制作更便捷，功能更丰富。
```

### 工作任务

```
1、ODS->ADS
		海豚调度
		Azkanban
2、制作图表
		 对外的图表
		 对内的图表
3、二开指标
		求新指标，京东商智、生意参谋、抖音罗盘、自己注册商家
		需要加上店铺 维度，自己梳理数据，梳理的表达的结构
		
4、自己定一个Hive函数，自定义UDTF
	输入1,2,3,4,5
		1
		2
		3
		4
		5
	get_json_object()
```

